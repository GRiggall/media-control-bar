#include <Encoder.h>
#include <Bounce.h>

Encoder myEnc(15, 14);

// Create Bounce objects for each button.  The Bounce object
// automatically deals with contact chatter or "bounce", and
// it makes detecting changes very simple.
Bounce button1 = Bounce(8, 10);  // 10 ms debounce time is appropriate
Bounce button2 = Bounce(7, 10);  // for most mechanical pushbuttons
Bounce button3 = Bounce(9, 10);
Bounce button4 = Bounce(10, 10);  // if a button is too "sensitive" 
Bounce button5 = Bounce(11, 10);  // you can increase this time.
Bounce button6 = Bounce(12, 10);
Bounce button7 = Bounce(16, 10);

void setup() {
  // Configure the pins for input mode with pullup resistors.
  // The pushbuttons connect from each pin to ground.  When
  // the button is pressed, the pin reads LOW because the button
  // shorts it to ground.  When released, the pin reads HIGH
  // because the pullup resistor connects to +5 volts inside
  // the chip.
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);
  pinMode(12, INPUT_PULLUP);
  pinMode(16, INPUT_PULLUP);
}

long oldPosition  = -999;
int enc_sensitivity = 3; // Higher number = more notches required to make a difference
int CW_movement = 0;
int CCW_movement = 0;

void loop() {
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) {
    if(newPosition > oldPosition) { // CW
      CCW_movement = 0;
      if(CW_movement >= enc_sensitivity){
        CW_movement = 0;
        Keyboard.press(KEY_MEDIA_VOLUME_INC);
        Keyboard.release(KEY_MEDIA_VOLUME_INC);
      }
      else {
        CW_movement ++;
      }
    }
    if(newPosition < oldPosition) { // CCW
      CW_movement = 0;
      if(CCW_movement >= enc_sensitivity){
        CCW_movement = 0;
        Keyboard.press(KEY_MEDIA_VOLUME_DEC);
        Keyboard.release(KEY_MEDIA_VOLUME_DEC);
      }
      else {
        CCW_movement ++;
      }
    }
    oldPosition = newPosition;
  }
  // Update all the buttons.  There should not be any long
  // delays in loop(), so this runs repetitively at a rate
  // faster than the buttons could be pressed and released.
  button1.update();
  button2.update();
  button3.update();
  button4.update();
  button5.update();
  button6.update();
  button7.update();

  // Check each button for "falling" edge.
  // falling = high (not pressed - voltage from pullup resistor)
  //           to low (pressed - button connects pin to ground)

  // Available keys here: https://www.pjrc.com/teensy/td_keyboard.html
  
  if (button1.fallingEdge()) {
    Keyboard.press(KEY_SYSTEM_POWER_DOWN);
    Keyboard.release(KEY_SYSTEM_POWER_DOWN);
  }
  if (button2.fallingEdge()) {
//    Keyboard.press(KEY_MEDIA_VOLUME_INC);
//    Keyboard.release(KEY_MEDIA_VOLUME_INC);
  }
  if (button3.fallingEdge()) {
    Keyboard.press(KEY_SPACE);
    Keyboard.release(KEY_SPACE);
  }
  if (button4.fallingEdge()) {
    Keyboard.press(KEY_MEDIA_PREV_TRACK);
    Keyboard.release(KEY_MEDIA_PREV_TRACK);
  }
  if (button5.fallingEdge()) {
    Keyboard.press(KEY_MEDIA_PLAY_PAUSE);
    Keyboard.release(KEY_MEDIA_PLAY_PAUSE);
  }
  if (button6.fallingEdge()) {
    Keyboard.press(KEY_MEDIA_NEXT_TRACK);
    Keyboard.release(KEY_MEDIA_NEXT_TRACK);
  }
  if (button7.fallingEdge()) {
    Keyboard.press(KEY_MEDIA_MUTE);
    Keyboard.release(KEY_MEDIA_MUTE);
  }
}
